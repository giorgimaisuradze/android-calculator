package com.maisuradze.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.lang.Exception
import java.math.BigDecimal

class MainActivity : AppCompatActivity() {

    private lateinit var result: TextView

    private var firstOperand: String = ""

    private var secondOperand: String = ""

    private var operator: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        result = findViewById(R.id.result)
    }

    fun onNumberClick(view: View) {
        if (view is Button) {
            if (operator == "") {
                firstOperand += view.text.toString()
            } else {
                secondOperand += view.text.toString()
            }

            draw()
        }
    }

    fun onOperationClick(view: View) {
        if (view is Button) {
            if (firstOperand != "" && secondOperand == "") {
                operator = view.text.toString()
            }

            draw()
        }
    }

    fun onAdditionalButtonClick(view: View) {
        if (view is Button) {
            if (view.text == "C") {
                clear()
            } else if (view.text == ".") {
                if (firstOperand != "" && operator == "" && !firstOperand.contains(".")) {
                    firstOperand += "."
                } else if(operator != "" && secondOperand != "" && !secondOperand.contains(".")) {
                    secondOperand += "."
                }
            }

            draw()
        }
    }

    fun onCalculate(view: View) {
        if (view is Button) {
            if (firstOperand.isBlank() || secondOperand.isBlank()) {
                Toast.makeText(this, "ERROR!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val calculatedResult: Double
                val firstNumber = firstOperand.toDouble();
                val secondNumber = secondOperand.toDouble();

                when (operator) {
                    "+" -> {
                        calculatedResult = firstNumber.plus(secondNumber)
                    }
                    "-" -> {
                        calculatedResult = firstNumber.minus(secondNumber)
                    }
                    "*" -> {
                        calculatedResult = firstNumber.times(secondNumber)
                    }
                    "/" -> {
                        calculatedResult = firstNumber.div(secondNumber)
                    }
                    else -> {
                        throw Exception("No valid operator")
                    }
                }

                clear()

                if(calculatedResult.isInfinite() || calculatedResult.isNaN()) {
                    result.text = calculatedResult.toString()
                } else {
                    firstOperand = calculatedResult.toBigDecimal().stripTrailingZeros().toPlainString()
                    draw()
                }
            }
        }
    }

    private fun draw() {
        result.text = firstOperand + operator + secondOperand
    }

    private fun clear() {
        firstOperand = ""
        secondOperand = ""
        operator = ""
    }

}